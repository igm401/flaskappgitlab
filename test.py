import unittest

import main


class TestMathForm(unittest.TestCase):
    def test_1(self):
        self.assertEqual(main.ravno(4, 5), [
            "Результат: -10.0"
        ])
        with self.assertRaises(TypeError):
            main.ravno("hello", 4)

    def test_2(self):
        self.assertEqual(main.bolshe(-14, 256, 3), "Результат: 5.0, -0.3333333333333333")
        with self.assertRaises(TypeError):
            main.bolshe("-14", 256, 3)

    def test_3(self):
        self.assertEqual(main.otris(), [
            "Результат: Нет корнет"
        ])


if __name__ == '__main__':
    unittest.main()